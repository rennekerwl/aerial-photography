import time
import os
import pygame
import RPi.GPIO as GPIO
from pygame import camera
from pygame.locals import *

##had wires on pins 9(ground), 11(led), 15(button), and 2(power)


def lightOn(pin):
    """This function turns on an LED."""
    GPIO.output(pin, GPIO.HIGH)
    
def lightOff(pin):
    """This function turns off an LED."""
    GPIO.output(pin, GPIO.LOW)

def snapshot(cam, waitTime):
    """This function takes a picture, saves the picture, and then waits for waitTime seconds."""
    filename = time.strftime("%a, %d, %b, %H:%M:%S")
    fileToSend = os.path.basename(filename + '.png')
    cam.start()
    image = cam.get_image()
    pygame.image.save(image, fileToSend)
    cam.stop()
    time.sleep(waitTime)
    
def checkSwitch(pin, switch):
    """To implement later"""
    ##Check for button goes here
    input = GPIO.input(pin)
    if input == True:
        if switch == True:
            return False
        elif switch == False:
            return True
    time.sleep(1)

def buttonCheck(state=False):
    """To implement later"""
    buttonState = GPIO.input(buttonPin)
    if buttonState:
        if not state:
            print("on")
            state = True
            lightOn(ledPin)
        else:
            print("off")
            state = False
            lightOff(ledPin)
    if(state == True):
        snapshot(cam, 5)
    time.sleep(1)

def main():
    """Testing thread."""
    state = False
    buttonPin = 15
    ledPin = 11
    
    #setup GPIO board
    GPIO.setmode(GPIO.BOARD)#define board configuration
    GPIO.setup(buttonPin, GPIO.IN)#set input channel for button
    GPIO.setup(ledPin, GPIO.OUT)#set input channel for button

    #setup camera
    pygame.init()
    pygame.camera.init()
    #resolutions = [(640,480), (800,600), (1024,768), (1280,1024), (1920,1080)]
    cam = pygame.camera.Camera("/dev/video0",(640,480))

    try:
        while True:
            buttonState = GPIO.input(buttonPin)
            if buttonState:
                if not state:
                    print("on")
                    state = True
                    lightOn(ledPin)
                else:
                    print("off")
                    state = False
                    lightOff(ledPin)
                    time.sleep(0.5)
            if state:
                print("Taking picture.")
                snapshot(cam, 5)

    #Clear GPIO pin bindings on keyboard interrupt
    except KeyboardInterrupt:
        GPIO.cleanup()



if __name__ == '__main__':
    main()
    
